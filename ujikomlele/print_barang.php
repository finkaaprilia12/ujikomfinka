<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 9%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Data Kelas</h1>
<table border="1" width="100%">
<tr>
	<th align="center">Id Inventaris</th>
	<th align="center"> Nama Barang</th>
	<th align="center"> Kondisi</th>
	<th align="center"> Keterangan</th>
	<th align="center"> Jumlah</th>
	<th align="center"> Jenis</th>
	<th align="center"> Tanggal Register</th>
	<th align="center"> Ruang</th>
	<th align="center"> Kode Inventaris</th>
	<th align="center"> Petugas</th>
</tr>
	<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn,"SELECT * FROM inventaris i
												left join jenis j on j.id_jenis=i.id_jenis
												left join ruang r on r.id_ruang=i.id_ruang
												left join petugas p on p.id_petugas=i.id_petugas
		");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr class ="success">
		
			<td><?php echo $data['id_inventaris']; ?></td>
			<td><?php echo $data['nama']; ?></td>
			<td><?php echo $data['kondisi']; ?></td>
			<td><?php echo $data['keterangan']; ?></td>
			<td><?php echo $data['jumlah']; ?></td>
			<td><?php echo $data['nama_jenis']; ?></td>
			<td><?php echo $data['tanggal_register']; ?></td>
			<td><?php echo $data['nama_ruang']; ?></td>
			<td><?php echo $data['kode_inventaris']; ?></td>
			<td><?php echo $data['nama_petugas']; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Inventaris.pdf', 'D');
?>