<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>POS (Point Of Sales)</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">POS </a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        <ul class="nav nav-icons">
                            <li class="active"><a href="#"><i class="icon-envelope"></i></a></li>
                            <li><a href="#"><i class="icon-eye-open"></i></a></li>
                            <li><a href="#"><i class="icon-bar-chart"></i></a></li>
                        </ul>
                        <form class="navbar-search pull-left input-append" action="#">
                        <input type="text" class="span3">
                        <button class="btn" type="button">
                            <i class="icon-search"></i>
                        </button>
                        </form>

                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Barang</a></li>
                                <li class="active"><a href="penjualan.php"><i class="menu-icon icon-dashboard"></i>Penjualan</a></li>
							</ul>
                            
                            <!--/.widget-nav-->
                            <ul class="widget widget-menu unstyled">
                                <li><a href="#"><i class="menu-icon icon-signout"></i>Logout </a></li>
                            </ul>
                        </div>
                        <!--/.sidebar-->
                    </div>
                    <!--/.span3-->
                    <div class="span9">
                        <div class="content">
                            <div class="btn-controls">
							<aside class="right-side">                
                <!-- Content Header (Page header) -->
                
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                            <div class="module">
							<div class="module-head">
								<h2>Penjualan</h2>
							</div>
							<br>
							<a href="input_penjualan.php"><button type="button" class="btn btn-outline btn-primary fa fa-plus"> Input </button></a>
							<br>
							<div class="module-body table">
								<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
									<thead>
										<tr>
											<th>No</th>
											<th>Kode Detail</th>
											<th>Tanggal Transaksi</th>
											<th>Total Transaksi</th>
											<th>Konsumen</th>
											<th>Aksi</th>
										</tr>
									</thead>
                                        <tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysql_query("SELECT * FROM penjualan");
												while($data=mysql_fetch_array($select))
												{
											?>
                        
											<tr class="success">
    									        <td><?php echo $no++; ?></td>
												<td><?php echo $data['id_detail'] ?></td>
												<td><?php echo $data['tgl_transaksi'] ?></td>
												<td><?php echo $data['total_transaksi'] ?></td>
												<td><?php echo $data['konsumen'] ?></td>
												<td><a href="edit_penjualan.php?id_penjualan=<?php echo $data['id_penjualan']; ?>"><button type="button" class="btn btn-outline btn-primary fa fa-edit"></button></a>
													<a href="hapus_penjualan.php?id_penjualan=<?php echo $data['id_penjualan']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash-o"></button></a>
												</td>
       										</tr>
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
            </div>
        </div>
    <script src="scripts/jquery-1.9.1.min.js"></script>
	<script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="scripts/datatables/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('.datatable-1').dataTable();
			$('.dataTables_paginate').addClass("btn-group datatable-pagination");
			$('.dataTables_paginate > a').wrapInner('<span />');
			$('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
			$('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
		} );
	</script>
    </body>
