<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>POS (Point Of Sales)</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
			<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">POS </a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        <ul class="nav nav-icons">
                            <li class="active"><a href="#"><i class="icon-envelope"></i></a></li>
                            <li><a href="#"><i class="icon-eye-open"></i></a></li>
                            <li><a href="#"><i class="icon-bar-chart"></i></a></li>
                        </ul>
                        <form class="navbar-search pull-left input-append" action="#">
                        <input type="text" class="span3">
                        <button class="btn" type="button">
                            <i class="icon-search"></i>
                        </button>
                        </form>

                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Barang</a></li>
                                <li class="active"><a href="penjualan.php"><i class="menu-icon icon-dashboard"></i>Penjualan</a></li>
							</ul>
                            
                            <!--/.widget-nav-->
                            <ul class="widget widget-menu unstyled">
                                <li><a href="#"><i class="menu-icon icon-signout"></i>Logout </a></li>
                            </ul>
                        </div>
                        <!--/.sidebar-->
                    </div>
                    <!--/.span3-->
					 <div class="span9">
                        <div class="content">
                            <div class="btn-controls">
							<aside class="right-side">
                            
                
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12"> 
					<div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head">
                                <h2>Input Penjualan</h2>
                            </div>
										<div class="box box-danger" id="loading-example">
											<div class="box-header">
												<h3 class="box-title">Cari Barang</h3>
											</div><!-- /.box-header -->
											
											<div class="box-body no-padding">
												<div class="container">
												<?php 
													include 'koneksi.php';
												?>
													<form method="get">
														
														<div class="input-group col-lg-3">
															<input type="text" name="cari" class="form-control">
															<span class="input-group-btn">
																<button type="text">
																<i class="icon-search"> Search </button></i>
															</span>
														</div>
														<br/>
													</form>
													
												</div><!-- /.row - inside box -->
											</div><!-- /.box-body -->
										</div><!-- /.box -->        

									</section>
					<div class="span9">
                    <div class="content">
                        <div class="module">
                            		
											<div class="box-body no-padding">
												<div class="container">
													<div class="col-lg-7">
  <?php 
   if(isset($_GET['cari'])){
   $cari = $_GET['cari'];
   echo "<b>Hasil pencarian : ".$cari."</b>";
  }
  ?>
    
  <table class="table table-bordered table-striped">
  
  <?php 
  if(isset($_GET['cari'])){
  $cari = $_GET['cari'];
  $data = mysql_query("select * from barang where kode_barang like '%".$cari."%'");   
  while($d = mysql_fetch_array($data)){ 
  ?>
  <tr>
   <td><?php echo $d['id_barang']; ?></td>
   <td><?php echo $d['kode_barang']; ?></td>
   <td><?php echo $d['deskripsi']; ?></td>
   <td><?php echo $d['harga_jual']; ?></td>
   <td><?php echo $d['stock']; ?></td>
   <td><input name="qty" type="number" placeholder="Masukan QTY" required autofocus /></td>
   <td><a href="simpan.php?id_barang=<?php echo $data['id_barang']; ?>"><button type="button"> Tambah </button></td>
  </tr>
  <?php } ?>
  <?php }else{
    $data = mysql_query("select * from barang '");  
    }
  //$no = 1;
  
  ?>
  
  </table>   
  <br/>
													</div> 
													
												</div><!-- /.row - inside box -->
											</div><!-- /.box-body -->
										</div><!-- /.box -->        

									</section>
									
									<section class="col-lg-12 connectedSortable"> 
										<!-- Box (with bar chart) -->
					<div class="span9">
                    <div class="content">
                        <div class="module">
										<div class="box box-warning" id="loading-example">
											<div class="box-header">
												<h3 class="box-title">Rincian Pembelian dan Stock Barang</h3>
											</div><!-- /.box-header -->
											
											<div class="box-body no-padding">
												<div class="container">
													<form class="col-lg-11">
														<table id="example" class="table table-bordered table-striped">
														<thead>
															<tr>
																<th>ID</th>
																<th>Kode Barang</th>
																<th>Nama Barang</th>
																<th>Harga</th>
																<th>Qty</th>
																<th>Total</th>
																<th>Aksi</th>
															</tr>
														</thead>
														<tbody>
 										<?php
 										include "koneksi.php";
 										$select=mysql_query("SELECT * FROM tblsementara LEFT JOIN barang ON tblsementara.id_barang = barang.id_barang  ");
 										while($data=mysql_fetch_array($select))
 										{
 										?>
 										<tr>
                    <td>
                      <input type="hidden" name="id_barang[]" value="<?php echo $data['id_sementara']; ?>">
                      <?php echo $data['id_sementara']; ?></td>
										<td>
					  <input type="hidden" name="id_barang[]" value="<?php echo $data['id_barang']; ?>">
                      <?php echo $data['kode_barang']; ?></td>
					  <td>
					  <input type="hidden" name="id_barang[]" value="<?php echo $data['id_barang']; ?>">
                      <?php echo $data['deskripsi']; ?></td>
										<td>
					  <input type="hidden" name="harga_jual[]" value="<?php echo $data['harga_jual']; ?>">
                      <?php echo "Rp. ". $data['harga_jual']; ?></td>
 										<td>
					  <input name="qty[]" id="qty" value="<?php echo $data['qty']; ?>" type="text" onfocus="mulaiHitung()" onblur="berhentiHitung()" required autofocus />
										</td>
										<td>
					  <input name="qty[]" id="qty" value="<?php echo $data['qty']; ?>" type="text" onfocus="mulaiHitung()" onblur="berhentiHitung()" required autofocus />
										</td>
						<td>
						<a href="hapus_sementara.php?id_tblsementara=<?php echo $data['id_tblsementara']; ?>" class="btn red"> Hapus </a> </td>
						</tr>

                  <?php } ?>
                  <tr>
                    <td colspan="4">Total Transaksi</td>
                    <td>
                      Rp. <?php
                    $grand_total = mysql_query("SELECT SUM(qty * harga_jual) AS TOTAL FROM tblsementara");
                      $rowGrandTotal = mysql_fetch_array($grand_total);
                     echo number_format($rowGrandTotal['TOTAL'], 0, ',', '.'); ?>
                       <input type="hidden" name="total[]" value="<?php echo $rowGrandTotal['TOTAL']; ?>">
                     </td>
                  </tr>
					<script type="text/javascript">
					function mulaiHitung(){
						Interval = setInterval("hitung()",1);
					}
					function hitung(){
						harga_jual = parseInt(docuent.getElementById("harga_jual").value);
						qty = parseInt(document.getElementById("qty").value);
						jumlah = harga_jual * qty;
					}
					function berhentiHitung(){
						clearInterval(Interval);
					}
					</script>
 									  </tbody>
                                    </table>
														
				<br/>
					<div class="input-field col s12">
                    <input class="btn green right" name="submit" type="submit" value="Bayar" class="validate">
                  </div><br/>
													</form>
													
												</div><!-- /.row - inside box -->
											</div><!-- /.box-body -->
										</div><!-- /.box -->        

									</section>
						
								</div>
								</div>
</div>
</div>
</div>
</div>
</div>
			</div>
</div>
</div>	
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
->
            </aside><!-- /.right-side -->

                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
            </div>
        </div>
     <script src="scripts/jquery-1.9.1.min.js"></script>
	<script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="scripts/datatables/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('.datatable-1').dataTable();
			$('.dataTables_paginate').addClass("btn-group datatable-pagination");
			$('.dataTables_paginate > a').wrapInner('<span />');
			$('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
			$('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
		} );
	</script>
    </body>
