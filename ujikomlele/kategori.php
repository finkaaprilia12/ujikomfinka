<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Latihan</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    
                    
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="home.php"><i class="menu-icon icon-dashboard"></i>Home
                                </a></li>
                                <li><a href="barang.php"><i class="menu-icon icon-bullhorn"></i>Barang</a>
                                </li>
                            </ul>
                            <!--/.widget-nav-->
                            
                            
                            <ul class="widget widget-menu unstyled">
                                <li><a href="kategori.php"><i class="menu-icon icon-bold"></i> Kategori </a></li>
                                <li><a href="peminjaman.php"><i class="menu-icon icon-book"></i>Peminjaman </a></li>
                                <li><a href="lihatuser.php"><i class="menu-icon icon-paste"></i>User </a></li>
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div>
                    <!--/.span3-->
                    <div class="span9">
                        <div class="content">
							<div class="module-body table">
							<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                        width="100%">
								 <div class="clearfix"></div>
									<p align="right"><a href="input_kategori.php"><button type="button" class="btn btn-outline btn-primary fa fa-plus"> Tambah Data </button>
									</p>
					<br>
								<div class="row">

									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="x_panel">
											<div class="x_title">
											 
												<h2>&nbsp;&nbsp;&nbsp;Data Kategori </h2>

												<div class="clearfix"></div>
											</div>

									<div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                
                                                <th> No </th>
                                                <th> Kategori</th>

                                                <th class=" no-link last"><span class="nobr">Aksi</span>
                                                </th>
                                            </tr>
                                        </thead>
								<tbody>
								<?php
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "inventori";
 
            // Membuat Koneksi
            $koneksi = new mysqli($servername, $username, $password, $dbname);
            
            // Melakukan Cek Koneksi
            if ($koneksi->connect_error) {
                die("Koneksi Gagal : " . $koneksi->connect_error);
            } 
 
            //Melakukan query
            $sql = "SELECT * FROM kategori";;
            $hasil = $koneksi->query($sql);
            $no = 1;
            if ($hasil->num_rows > 0) {
                foreach ($hasil as $row) { ?>
                  <tr>     
                  <td><?php echo $no; ?></td>
                  <td><?php echo $row['kategori']; ?></td>
                  <td>
				  <a href="hapus_kategori.php?id_kategori=<?php echo $row['id_kategori']; ?>"><button type="button" class="btn btn-danger"> Hapus</button></td>
				  </tr>
            <?php 
            $no++; 
            } 
              } else { 
            echo "0 results"; 
              } $koneksi->close(); 
            ?>
								
							</tbody>
                                                       </table>
						<script type ="text/javascript" src="assets/js/jquery.min.js"></script>
						<script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
						<script>$(document).ready(function(){
							$('#example').DataTable();
						});
						</script>
						
                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />

                    </div>
					 <script type="text/javascript">
                        $(document).ready(function () {
                            $('#birthday').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_4"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>

							</div>
                            <!--/#btn-controls-->
                            <!--/.module-->
                            
                            <!--/.module-->
                        </div>
                        <!--/.content-->
                    </div>
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
            </div>
        </div>
        <script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="scripts/common.js" type="text/javascript"></script>
      
    </body>
