<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inventaris SMK </title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Inventaris SMK </a>
                    
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
          <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
					<?php
						if ($_SESSION['id_level']==1) {
							
                        echo'<div class="sidebar">
						
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-tasks"></i>Inventarisir
                                </a></li>
                                <li><a href="peminjam.php"><i class="menu-icon icon-share"></i>Peminjaman</a>
                                </li>
								<li><a href="pengembalian.php"><i class="menu-icon icon-reply"></i> Pengembalian </a></li>
                                <li><a href="laporan.php"><i class="menu-icon icon-book"></i>Laporan </a></li>
								
                            </ul>
                            <!--/.widget-nav-->
                            <ul class="widget widget-menu unstyled">
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
								</i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
								</i>Lainnya </a>
									<ul id="togglePages" class="collpase unstyled">
										<li><a href="jenis.php"><i class="icon-inbox"></i>Jenis </a></li>
										<li><a href="ruang.php"><i class="icon-inbox"></i>Ruang </a></li>
										<li><a href="petugas.php"><i class="icon-inbox"></i>Petugas </a></li>
										<li><a href="pegawai.php"><i class="icon-inbox"></i>Pegawai </a></li>
										<li><a href="level.php"><i class="icon-inbox"></i>Level </a></li>
										
									</ul>
								
                                <li><a href="logout.php"><i class="menu-icon icon-undo"></i>Logout </a></li>
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div><!----penutup---->';
					
						}else if ($_SESSION['id_level']==2) {
							
						echo'<div class="sidebar">
						
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Inventarisir</a></li>
                                <li><a href="peminjam.php"><i class="menu-icon icon-bullhorn"></i>Peminjaman</a>
                                </li>
								<li><a href="pengembalian.php"><i class="menu-icon icon-bold"></i> Pengembalian </a></li>
                                
								
                            </ul>
                            <!--/.widget-nav-->
                            <ul class="widget widget-menu unstyled">
								<li><a href="logout.php">Logout </a></li>
									
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div><!----penutup---->';
					
						}else if ($_SESSION['id_level']==3) {
						echo'<div class="sidebar">
						
                            <ul class="widget widget-menu unstyled">
                                
                                <li><a href="peminjam.php"><i class="menu-icon icon-bullhorn"></i>Peminjaman</a></li>
                                <li><a href="logout.php"><i class="menu-icon icon-paste"></i>Logout </a></li>
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div><!----penutup---->';
					}
					?>
							
                    <!--/.span3-->
          <div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Pengembalian</h3>
							</div>
							<div class="module-body">
					<div class="box-content">
					<center><div class="panel-body">
                        <div class="col-lg-3 col-md-offset-4">
                <label>Pilih Id Peminjaman</label>
                <form method="POST">
                    <select name="id_peminjaman" class="form-control m-bot15">
                        <?php
                        include "koneksi.php";
                                //display values in combobox/dropdown
                        $result = mysql_query("SELECT id_peminjaman from peminjaman where status_peminjaman='pinjam' ");
                        while($row = mysql_fetch_assoc($result))
                        {
                            echo "<option value='$row[id_peminjaman]'>$row[id_peminjaman]</option>";
                        } 
                        ?>
                   </select>
                                    <br/>
                                <button type="submit" name="pilih" class="btn btn-outline btn-primary">Tampilkan</button>
                            </form>
                        </div>
                    </div>
					    <?php
                if(isset($_POST['pilih'])){?>
                  <form action="proses_pengembalian.php" method="post" enctype="multipart/form-data">
                     <?php
                     include "koneksi.php";
                     $id_peminjaman=$_POST['id_peminjaman'];
                     $select=mysql_query("select * from peminjaman i left join detail_pinjam p on p.id_detail_pinjam=i.id_peminjaman
                        where id_peminjaman='$id_peminjaman'");
                     while($data=mysql_fetch_array($select)){
                        ?>
                <div class="form-group">
                    <label>Id Inventaris</label>
                    <input name="id_peminjaman" type="hidden" class="form-control"  placeholder="Masukan Jumlah barang" value="<?php echo $data['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_detail_pinjam" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_inventaris" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="">
                </div><br>                    
               
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Tanggal Pinjam
                    </label>
                    <div class="col-md-6 col-sm-6o col-xs-12">
                        <input type="datetime" name="" value="<?php echo $data['tanggal_pinjam']?>" class="form-control col-md-7 col-xs-12"  placeholder="tanggal_pinjam">
                        <input type="hidden" name="tanggal_pinjam" value="<?php echo $data['tanggal_pinjam']?>" >
                        <input type="hidden" name="tanggal_kembali" >
                    </div>
                </div><br><br><br>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        ID Pegawai
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="" value="<?php echo $data['id_pegawai']?>" class="form-control col-md-7 col-xs-12"  placeholder="ID Pegawai">
                        <input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai']?>" >
                    </div>
                </div><br><br><br>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Jumlah
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" name="jumlah_pinjam" value="<?php echo $data['jumlah_pinjam']?>" class="form-control col-md-7 col-xs-12"  placeholder="Jumlah">
                    </div>
                </div><br><br>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                     Status Peminjaman
                 </label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="status_peminjaman" class="form-control m-bot15">
                    <option><?php echo $data['status_peminjaman']?></option>
                    <option>Kembali</option>

                </select>
            </div><br><br><br>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>

    <?php } ?>
</form>

<?php } ?>
<div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal Pinjam</th>
                                                    <th>Tanggal Kembali</th>
                                                    <th>Status Peminjaman</th>
                                                    <th>Nama Pegawai</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php
								include "koneksi.php";
								$no=1;
								$select=mysqli_query($conn,"select * from peminjaman a left join pegawai c on c.id_pegawai=a.id_pegawai where status_peminjaman='Dikembalikan'");
								while($data=mysqli_fetch_array($select))
										{
								?>
                        
                                <tr class="success">
									<td><?php echo $no++; ?></td>
                                    <td><?php echo $data['tanggal_pinjam'] ?></td>
									<td><?php echo $data['tanggal_kembali'] ?></td>
									<td><?php echo $data['status_peminjaman'] ?></td>
									<td><?php echo $data['nama_pegawai'] ?></td>
                                </tr>
								<?php } ?>
                                            
                                            </tbody>
                                        </table>
									<script type ="text/javascript" src="assets/js/jquery.min.js"></script>
									<script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
									<script>$(document).ready(function(){
											$('#example').DataTable();
											});
									</script>
                                    </div>
                                    <!-- /.table-responsive -->
                                   </div>
					</div></center>
				</div><!--/span-->
			
			</div><!--/row-->

	</div><!--/.fluid-container-->
						
                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />

                    </div>
					 <script type="text/javascript">
                        $(document).ready(function () {
                            $('#birthday').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_4"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>

							</div>
                            <!--/#btn-controls-->
                            <!--/.module-->
                            
                            <!--/.module-->
                        </div>
                        <!--/.content-->
                    </div>
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
            </div>
        </div>
       <script src="scripts/jquery-1.9.1.min.js"></script>
	<script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="scripts/datatables/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('.datatable-1').dataTable();
			$('.dataTables_paginate').addClass("btn-group datatable-pagination");
			$('.dataTables_paginate > a').wrapInner('<span />');
			$('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
			$('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
		} );
	</script>
    </body>
