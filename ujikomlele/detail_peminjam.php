<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inventaris SMK </title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Inventaris SMK </a>
                    
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
          <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
					<?php
						if ($_SESSION['id_level']==1) {
							
                        echo'<div class="sidebar">
						
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-tasks"></i>Inventarisir
                                </a></li>
                                <li><a href="peminjaman.php"><i class="menu-icon icon-share"></i>Peminjaman</a>
                                </li>
								<li><a href="pengembalian.php"><i class="menu-icon icon-reply"></i> Pengembalian </a></li>
                                <li><a href="laporan.php"><i class="menu-icon icon-book"></i>Laporan </a></li>
								
                            </ul>
                            <!--/.widget-nav-->
                            <ul class="widget widget-menu unstyled">
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
								</i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
								</i>Lainnya </a>
									<ul id="togglePages" class="collpase unstyled">
										<li><a href="jenis.php"><i class="icon-inbox"></i>Jenis </a></li>
										<li><a href="ruang.php"><i class="icon-inbox"></i>Ruang </a></li>
										<li><a href="petugas.php"><i class="icon-inbox"></i>Petugas </a></li>
										<li><a href="pegawai.php"><i class="icon-inbox"></i>Pegawai </a></li>
										<li><a href="level.php"><i class="icon-inbox"></i>Level </a></li>
										
									</ul>
								
                                <li><a href="logout.php"><i class="menu-icon icon-undo"></i>Logout </a></li>
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div><!----penutup---->';
					
						}else if ($_SESSION['id_level']==2) {
							
						echo'<div class="sidebar">
						
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Inventarisir</a></li>
                                <li><a href="peminjam.php"><i class="menu-icon icon-bullhorn"></i>Peminjaman</a>
                                </li>
								<li><a href="pengembalian.php"><i class="menu-icon icon-bold"></i> Pengembalian </a></li>
                                
								
                            </ul>
                            <!--/.widget-nav-->
                            <ul class="widget widget-menu unstyled">
								<li><a href="logout.php">Logout </a></li>
									
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div><!----penutup---->';
					
						}else if ($_SESSION['id_level']==3) {
						echo'<div class="sidebar">
						
                            <ul class="widget widget-menu unstyled">
                                
                                <li><a href="peminjam.php"><i class="menu-icon icon-bullhorn"></i>Peminjaman</a></li>
                                <li><a href="logout.php"><i class="menu-icon icon-paste"></i>Logout </a></li>
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div><!----penutup---->';
					}
					?>
						
				</div>	
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Detail Pinjam 
                                </div>
                                <!-- /.panel-heading -->
	                            <div class="panel-body">
								<?php
								include "koneksi.php";
								$id_peminjaman=$_GET['id_peminjaman'];
								$select=mysqli_query($koneksi, "select * from peminjaman left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
																				left join detail_pinjam on peminjaman.id_peminjaman=detail_pinjam.id_detail_pinjam
																				left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris
																				where id_peminjaman='$id_peminjaman'");
																				while($row=mysqli_fetch_array($select)){
								?>
                                        <table class="table">
                <tr>
                    <td>Nama Barang</td>
                    <td>:</td>
                    <td><?php echo $row['nama']; ?></td>
                </tr>
                <tr>
                    <td>Jumlah Pinjam</td>
                    <td>:</td>
                    <td><?php echo $row['jumlah']; ?></td>
                </tr>
				<tr>
                    <td>Tanggal Pinjam</td>
                    <td>:</td>
                    <td><?php echo $row['tanggal_pinjam']; ?></td>
                </tr>
				<tr>
                    <td>Status Peminjaman</td>
                    <td>:</td>
                    <td><?php echo $row['status_peminjaman']; ?></td>
                </tr>
				<tr>
                    <td>Nama Pegawai</td>
                    <td>:</td>
                    <td><?php echo $row['nama_pegawai']; ?></td>
                </tr>
																				<?php } ?>
            </table>
			
								
							<div class="form-group">
								<a href="peminjaman.php" ><button type="button" class="btn btn-outline btn-primary fa fa-reply"> Kembali</button></a>
							</div>
                                   </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>

    </body>
</html>
