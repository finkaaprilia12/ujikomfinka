<?php
include "koneksi.php";
$id_barang=$_GET['id_barang'];

$select=mysql_query("select * from barang");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>POS (Point Of Sales)</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">POS </a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        <ul class="nav nav-icons">
                            <li class="active"><a href="#"><i class="icon-envelope"></i></a></li>
                            <li><a href="#"><i class="icon-eye-open"></i></a></li>
                            <li><a href="#"><i class="icon-bar-chart"></i></a></li>
                        </ul>
                        <form class="navbar-search pull-left input-append" action="#">
                        <input type="text" class="span3">
                        <button class="btn" type="button">
                            <i class="icon-search"></i>
                        </button>
                        </form>

                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Barang</a></li>
                                <li class="active"><a href="penjualan.php"><i class="menu-icon icon-dashboard"></i>Penjualan</a></li>
							</ul>
                            
                            <!--/.widget-nav-->
                            <ul class="widget widget-menu unstyled">
                                <li><a href="#"><i class="menu-icon icon-signout"></i>Logout </a></li>
                            </ul>
                        </div>
                        <!--/.sidebar-->
                    </div>
                    <!--/.span3-->
                    <div class="span9">
                        <div class="content">
                            <div class="btn-controls">
							<aside class="right-side">                
                <!-- Content Header (Page header) -->
                
                <!-- Main content -->
                                <section class="content">
                    <div class="row">
                        <div class="col-xs-11">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Input Data Barang</h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                     <form role="form" method="post" action="update_barang.php">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label> ID Barang </label>
                                            <input name="id_barang" value="<?php echo $data['id_barang'];?>" type="text" class="form-control" placeholder="Masukan Kode Barang" readonli />
                                        </div>
										<div class="form-group">
                                            <label> Kode Barang </label>
                                            <input name="kode_barang" value="<?php echo $data['kode_barang'];?>" type="text" class="form-control" placeholder="Masukan Kode Barang" required autofocus />
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Barang</label>
                                            <input name="deskripsi" value="<?php echo $data['deskripsi'];?>" type="text" class="form-control" placeholder="Masukan Nama Barang" required autofocus />
                                        </div>
										<div class="form-group">
                                            <label>Stock</label>
                                            <input name="stock" value="<?php echo $data['stock'];?>" type="number" class="form-control" placeholder="Masukan Stock" required autofocus />
                                        </div>
										<div class="form-group">
                                            <label>Harga Beli</label>
                                            <input name="harga_beli" value="<?php echo $data['harga_beli'];?>" type="text" class="form-control" placeholder="Masukan Harga Beli" required autofocus />
                                        </div>
										<div class="form-group">
                                            <label>Harga Jual</label>
                                            <input name="harga_jual" value="<?php echo $data['harga_jual'];?>" type="text" class="form-control" placeholder="Masukan Harga Jual" required autofocus />
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-outline btn-primary">Submit</button>
										<button type="reset" class="btn btn-outline btn-danger">Reset</button>
                                    </div>
                                </form>
									
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
            </div>
        </div>
    <script src="scripts/jquery-1.9.1.min.js"></script>
	<script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="scripts/datatables/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('.datatable-1').dataTable();
			$('.dataTables_paginate').addClass("btn-group datatable-pagination");
			$('.dataTables_paginate > a').wrapInner('<span />');
			$('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
			$('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
		} );
	</script>
    </body>
