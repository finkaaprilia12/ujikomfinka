
<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inventaris SMK </title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Inventaris SMK </a><br/>
                   <p align="right"> <a href="../login.php"><button type="button" class="btn-black fa fa-plus"> Login </button></a></p>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
						
                    <!--/.span3-->
                 	<div class="span9">
                        <div class="content">
                            
                                    
                        
                               
                                <div class="sm-12">
								<p align="center"><img src="images/user.png"/></p>
								<br/>
                                   <p>Setiap satuan pendidikan wajib memiliki sarana yang meliputi perabot, peralatan pendidikan, media pendidikan, buku dan sumber belajar lainnya, bahan habis pakai, serta perlengkapan lain yang diperlukan untuk menunjang proses pembelajaran yang teratur dan berkelanjutan.
Setiap satuan pendidikan wajib memiliki prasarana yang meliputi lahan, ruang kelas, ruang pimpinan satuan pendidikan, ruang pendidik, ruang tata usaha, ruang perpustakaan, ruang laboratorium, ruang bengkel kerja, ruang unit produksi, ruang kantin, instalasi daya dan jasa, tempat berolahraga, tempat beribadah, tempat bermain, tempat berkreasi, dan ruang/tempat lain yang diperlukan untuk menunjang proses pembelajaran yang teratur dan berkelanjutan.
</p>
						
                                </div>
                          
                            <!--/.module-->
                        </div>
                        <!--/.content-->
                    </div>
							</tbody>
						
							
                                                       </table>
						<script type ="text/javascript" src="assets/js/jquery.min.js"></script>
						<script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
						<script>$(document).ready(function(){
							$('#example12').DataTable();
						});
						
                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />

                    </div>	
					 <script type="text/javascript">
                        $(document).ready(function () {
                            $('#birthday').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_4"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>

							</div>
                            <!--/#btn-controls-->
                            <!--/.module-->
                            
                            <!--/.module-->
                        </div>
                        <!--/.content-->
                    </div>
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
            </div>
        </div>
       <script src="scripts/jquery-1.9.1.min.js"></script>
	<script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="scripts/datatables/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('.datatable-1').dataTable();
			$('.dataTables_paginate').addClass("btn-group datatable-pagination");
			$('.dataTables_paginate > a').wrapInner('<span />');
			$('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
			$('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
		} );
	</script>
    </body>
