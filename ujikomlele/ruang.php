<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inventaris SMK </title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Inventaris SMK </a>
                    
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-tasks"></i>Inventarisir
                                </a></li>
                                <li><a href="peminjaman.php"><i class="menu-icon icon-share"></i>Peminjaman</a>
                                </li>
								<li><a href="pengembalian.php"><i class="menu-icon icon-reply"></i> Pengembalian </a></li>
                                <li><a href="laporan.php"><i class="menu-icon icon-book"></i>Laporan </a></li>
								
                            </ul>
                            </ul>
                            <!--/.widget-nav-->
                            
                            
                           <ul class="widget widget-menu unstyled">
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
								</i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
								</i>Lainnya </a>
									<ul id="togglePages" class="collpase unstyled">
										<li><a href="jenis.php"><i class="icon-inbox"></i>Jenis </a></li>
										<li><a href="ruang.php"><i class="icon-inbox"></i>Ruang </a></li>
										<li><a href="petugas.php"><i class="icon-inbox"></i>Petugas </a></li>
										<li><a href="pegawai.php"><i class="icon-inbox"></i>Pegawai </a></li>
										<li><a href="level.php"><i class="icon-inbox"></i>Level </a></li>
									</ul>
								
                                <li><a href="logout.php"><i class="menu-icon icon-paste"></i>Logout </a></li>
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div>
                    <!--/.span3-->
                    <div class="span9">
                        <div class="content">
                            
                                    
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Data Ruang</h3>
                                </div>
                                <div class="module-body table">
                                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                        width="100%">
                                        <thead>
                                            <tr class="headings">
                                                
                                                <th> No </th>
												<th> Nama Ruang</th>
												<th> Kode Ruang</th>
												<th> Keterangan</th>
                                                <th class=" no-link last"><span class="nobr">Aksi</span>
                                                </th>
                                            </tr>
                                        </thead>
								<tbody>
								<?php
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "ujikomlele";
 
            // Membuat Koneksi
            $koneksi = new mysqli($servername, $username, $password, $dbname);
            
            // Melakukan Cek Koneksi
            if ($koneksi->connect_error) {
                die("Koneksi Gagal : " . $koneksi->connect_error);
            } 
 
            //Melakukan query
            $sql = "SELECT * FROM ruang";;
            $hasil = $koneksi->query($sql);
            $no = 1;
            if ($hasil->num_rows > 0) {
                foreach ($hasil as $row) { ?>
                  <tr>     
                  <td><?php echo $no; ?></td>
				  <td><?php echo $row['nama_ruang']; ?></td>
				  <td><?php echo $row['kode_ruang']; ?></td>
				  <td><?php echo $row['keterangan']; ?></td>
                  <td>
				  <a href="hapus_ruang.php?id_ruang=<?php echo $row['id_ruang']; ?>"><button type="button" class="btn btn-danger"> Hapus</button>
				   <a href="edit_ruang.php?id_ruang=<?php echo $row['id_ruang']; ?>"><button type="button" class="btn btn-info"> Edit</button></td>
				  </tr>
            <?php 
            $no++; 
            } 
              } else { 
            echo "0 results"; 
              } $koneksi->close(); 
            ?>
								
							</tbody>
                                                       </table>
						<script type ="text/javascript" src="assets/js/jquery.min.js"></script>
						<script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
						<script>$(document).ready(function(){
							$('#example12').DataTable();
						});
						</script>
						<a href="input_ruang.php"><button type="button" class="btn-primary fa fa-plus"> Tambah </button></a>
						</script><a href="print_excel_ruang.php"><button type="button" class="btn-primary fa fa-plus"> EXPORT EXCEL </button></a>						
						</script><a href="print_pdf_ruang.php"><button type="button" class="btn-primary fa fa-plus"> EXPORT PDF</button></a>
                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />

                    </div>
					 <script type="text/javascript">
                        $(document).ready(function () {
                            $('#birthday').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_4"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>

							</div>
                            <!--/#btn-controls-->
                            <!--/.module-->
                            
                            <!--/.module-->
                        </div>
                        <!--/.content-->
                    </div>
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
            </div>
        </div>
       <script src="scripts/jquery-1.9.1.min.js"></script>
	<script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="scripts/datatables/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('.datatable-1').dataTable();
			$('.dataTables_paginate').addClass("btn-group datatable-pagination");
			$('.dataTables_paginate > a').wrapInner('<span />');
			$('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
			$('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
		} );
	</script>
    </body>
