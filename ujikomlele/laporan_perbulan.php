<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inventaris SMK </title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
        <link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Inventaris SMK </a>
                    
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                               <li class="active"><a href="index.php"><i class="menu-icon icon-tasks"></i>Inventarisir
                                </a></li>
                                <li><a href="peminjaman.php"><i class="menu-icon icon-share"></i>Peminjaman</a>
                                </li>
                                <li><a href="pengembalian.php"><i class="menu-icon icon-reply"></i> Pengembalian </a></li>
                                <li><a href="laporan.php"><i class="menu-icon icon-book"></i>Laporan </a></li>
                            </ul>
                            <!--/.widget-nav-->
                            
                            
                           <ul class="widget widget-menu unstyled">
                                <li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Lainnya </a>
                                    <ul id="togglePages" class="collpase unstyled">
                                        <li><a href="jenis.php"><i class="icon-inbox"></i>Jenis </a></li>
                                        <li><a href="ruang.php"><i class="icon-inbox"></i>Ruang </a></li>
                                        <li><a href="petugas.php"><i class="icon-inbox"></i>Petugas </a></li>
                                        <li><a href="pegawai.php"><i class="icon-inbox"></i>Pegawai </a></li>
                                        <li><a href="level.php"><i class="icon-inbox"></i>Level </a></li>
                                        
                                    </ul>
                                
                                <li><a href="logout.php"><i class="menu-icon icon-paste"></i>Logout </a></li>
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div>
                    <!--/.span3-->
                    <div class="span9">
                        <div class="content">
                            
                                    
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Data laporan</h3>
                                </div>
                                <div class="module-body table">
               
                                <div class="col-lg-5">
                                        <label>Pilih Bulan</label>
											<form method="get">
											<select name="bulan" class="form-control m-bot15">
												<?php
												include "koneksi.php";
												//display values in combobox/dropdown
												$result = mysqli_query($koneksi,"SELECT month(tanggal_pinjam) as bulan FROM peminjaman GROUP BY MONTH(tanggal_pinjam)");
												while($row = mysqli_fetch_assoc($result))
												{
												echo "<option value='$row[bulan]'>$row[bulan]</option>";
												} 
												?>
											</select>
													<br/>
												<button type="submit" class="btn btn-outline btn-primary">Tampilkan</button>
											</form>
										</div>
								 </div>

						 <?php if(isset($_GET['bulan'])) { ?>
						 
						 <div class="box-content">
								<table class="table table-striped table-bordered bootstrap-datatable datatable">
											<thead> 
														 <tr>
															<th>No.</th>
															<th>ID Peminjaman</th>
															<th>Tanggal Pinjam</th>
															<th>Tanggal Kembali</th>
															<th>Nama Pegawai</th>
												
															</tr> 
											</thead>   
											<tbody>
											<?php
									$no=1;
										$select=mysqli_query($koneksi,"select * from peminjaman a left join pegawai b on b.id_pegawai=a.id_pegawai
																left join detail_pinjam d on a.id=d.id_detail_pinjam
																left join inventaris i on i.id_inventaris=d.id_inventaris WHERE MONTH(tanggal_pinjam) = '$_GET[bulan]'");
									
												while($data=mysqli_fetch_array($select))
												{
												?>
												  <tr>
													<td><?php echo $no++; ?></td>
													<td><?php echo $data['id']; ?></td>
													<td><?php echo $data['tanggal_pinjam']; ?></td>
													<td><?php echo $data['tanggal_kembali']; ?></td>
													<td><?php echo $data['nama_pegawai']; ?></td>
													</tr>
													<?php
												}
												?>
											</tbody>
								</table>  
								 <a href="laporan_pertanggal.php"><button type="submit" class="btn btn-primary">Per Tanggal </button></a>
								 <a href="laporan_perbulan.php"><button type="submit" class="btn btn-primary">Per Bulan </button></a>
								 <a href="laporan_pertahun.php"><button type="submit" class="btn btn-primary">Per Tahun </button></a>
							</div>
					
							</div>
							
							<?php
							}
							?>
								<table id="example" class="table table-striped table-bordered table-hover">
								<thead>
							
										</tbody>
									</table>
									<script type="text/javascript" src="assets/js/jquery.min.js"></script>
									<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
									<script>$(document).ready(function(){
										$('#example').DataTable();
									});
									</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
    <!-- /#wrapper -->

    <script type="text/javascript">
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
                traffic = [
                {
                    Source: "Direct", Amount: 323, Change: 53, Percent: 23, Target: 600
                },
                {
                    Source: "Refer", Amount: 345, Change: 34, Percent: 45, Target: 567
                },
                {
                    Source: "Social", Amount: 567, Change: 67, Percent: 23, Target: 456
                },
                {
                    Source: "Search", Amount: 234, Change: 23, Percent: 56, Target: 890
                },
                {
                    Source: "Internal", Amount: 111, Change: 78, Percent: 12, Target: 345
                }];


            $("#shieldui-chart1").shieldChart({
                theme: "dark",

                primaryHeader: {
                    text: "Visitors"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "area",
                    collectionAlias: "Q Data",
                    data: performance
                }]
            });

            $("#shieldui-chart2").shieldChart({
                theme: "dark",
                primaryHeader: {
                    text: "Traffic Per week"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "pie",
                    collectionAlias: "traffic",
                    data: visits
                }]
            });

            $("#shieldui-grid1").shieldGrid({
                dataSource: {
                    data: traffic
                },
                sorting: {
                    multiple: true
                },
                rowHover: false,
                paging: false,
                columns: [
                { field: "Source", width: "170px", title: "Source" },
                { field: "Amount", title: "Amount" },                
                { field: "Percent", title: "Percent", format: "{0} %" },
                { field: "Target", title: "Target" },
                ]
            });            
        });        
    </script>
</body>
</html>
 