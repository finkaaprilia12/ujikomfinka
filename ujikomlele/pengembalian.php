<?php
include "cek.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inventaris SMK </title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Inventaris SMK </a>
                    
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
					<?php
						if ($_SESSION['id_level']==1) {
							
                        echo'<div class="sidebar">
						
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-tasks"></i>Inventarisir
                                </a></li>
                                <li><a href="peminjaman.php"><i class="menu-icon icon-share"></i>Peminjaman</a>
                                </li>
								<li><a href="pengembalian.php"><i class="menu-icon icon-reply"></i> Pengembalian </a></li>
                                <li><a href="laporan.php"><i class="menu-icon icon-book"></i>Laporan </a></li>
								
                            </ul>
                            <!--/.widget-nav-->
                            <ul class="widget widget-menu unstyled">
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
								</i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
								</i>Lainnya </a>
									<ul id="togglePages" class="collpase unstyled">
										<li><a href="jenis.php"><i class="icon-inbox"></i>Jenis </a></li>
										<li><a href="ruang.php"><i class="icon-inbox"></i>Ruang </a></li>
										<li><a href="petugas.php"><i class="icon-inbox"></i>Petugas </a></li>
										<li><a href="pegawai.php"><i class="icon-inbox"></i>Pegawai </a></li>
										<li><a href="level.php"><i class="icon-inbox"></i>Level </a></li>
										
									</ul>
								
                                <li><a href="logout.php"><i class="menu-icon icon-undo"></i>Logout </a></li>
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div><!----penutup---->';
					
						}else if ($_SESSION['id_level']==2) {
							
						echo'<div class="sidebar">
						
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-tasks"></i>Inventarisir
                                </a></li>
                                <li><a href="peminjaman.php"><i class="menu-icon icon-share"></i>Peminjaman</a>
                                </li>
								<li><a href="pengembalian.php"><i class="menu-icon icon-reply"></i> Pengembalian </a></li>
                                
								
                            </ul>
                            <!--/.widget-nav-->
                            <ul class="widget widget-menu unstyled">
								<li><a href="logout.php">Logout </a></li>
									
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div><!----penutup---->';
					
						}else if ($_SESSION['id_level']==3) {
						echo'<div class="sidebar">
						
                            <ul class="widget widget-menu unstyled">
                                
                                <li><a href="peminjam3.php"><i class="menu-icon icon-share"></i>Peminjaman</a>
                                <li><a href="logout.php"><i class="menu-icon icon-paste"></i>Logout </a></li>
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div><!----penutup---->';
					}
					?>
							


							<!--/span-->
                <div class="span9" id="content">
                    <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Data Peminjaman</h3>
                                </div>
                               <div class="box-content">
							     <div class="row-fluid">
                        <!-- block -->
						
					<h5> pilih data </h5>
							<form method="POST">
							<select name="id" class="span8 typeahead">
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($koneksi, "SELECT id from peminjaman where status_peminjaman='Pinjam'");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option value='$row[id]'>$row[id]</option>";
								} 
								?>
									</select>
									<br/>
								<button type="submit" name="submit" class="btn btn-outline btn-primary">Data</button>
							</form>
						</div>
						
						<br/>
						
						
					<?php
						if(isset($_POST['submit'])){?>
								
							
                               <form action="proses_kembali.php" method="post" role="form">
							            <?php
										include "koneksi.php";
										$id=$_POST['id'];
										$select=mysqli_query($koneksi, "SELECT * from peminjaman left join detail_pinjam on peminjaman.id=detail_pinjam.id_detail_pinjam
																							  left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
																							  left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris
																							where id='$id	'");
																							
										while($data=mysqli_fetch_array($select)){
										?>
										
										<div class="control-group">
										<input type="hidden" name="id" value="<?php echo $id ?>">
											<label class="control-label" for="disabledInput">ID Peminjaman</label>
											<div class="controls">
												<input name="id" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['id'];?>" type="text" placeholder="Disabled input here…" readonly="">
												<input name="id_detail_pinjam" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['id_detail_pinjam'];?>" type="hidden" placeholder="Disabled input here…">
											</div>	
										</div>
										<div class="control-group">
											<label class="control-label" for="disabledInput">Tanggal Pinjam</label>
											<div class="controls">
												<input name="tanggal_pinjam" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['tanggal_pinjam'];?>" type="text" placeholder="Disabled input here…">
												<input name="tanggal_kembali" type="hidden" class="input-xlarge disabled" id="disabledInput" placeholder="Disabled input here…" disabled="">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="disabledInput">Pegawai</label>
											<div class="controls">
												<input name="id_pegawai" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['id_pegawai'];?>.<?php echo $data['nama_pegawai'];?>" type="text" placeholder="Disabled input here…">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="disabledInput">Nama Barang</label>
											<div class="controls">
												<input name="id" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['id'];?>" type="text" placeholder="Disabled input here…">
												<input name="nama" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['id_inventaris'];?>" type="text" placeholder="Disabled input here…">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="disabledInput">Jumlah</label>
											<div class="controls">
												<input name="jumlah" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['jumlah'];?>" type="text">
											</div>
										</div>
										<div class="control-group">
												<label class="control-label" for="basicinput"><b> Status </b></label>
												<select class="span8 typeahead"  name="status_peminjaman" required>
													<option value="">Pilih status</option>
                                                        <option>Kembali</option>
												</select>
											</div>
										
										<div class="box-footer">
                                        <button type="submit" class="btn btn-outline btn-primary">Submit</button>
                                    </div>
										<?php } ?>
                                 </form>
								<?php } ?>
								
								<table class="table table-striped table-bordered bootstrap-datatable datatable">
											<thead> 
														<tr>
															<th>No</th>
															<th>Tanggal Pinjam</th>
															<th>Tanggal Kembali</th>
															<th>Status Peminjaman</th>
															<th>Nama Pegawai</th>
														</tr>
											</thead>   
											<tbody>
														<?php
													include "koneksi.php";
													$no=1;
													$select=mysqli_query($koneksi,"select * from peminjaman a left join pegawai c on c.id_pegawai=a.id_pegawai where status_peminjaman='kembali'");
													while($data=mysqli_fetch_array($select))
													{
														?>
															<tr class="even pointer">
																
																<td> <?php echo $no++; ?> </td>
																<td> <?php echo $data['tanggal_pinjam']; ?> </td>
																<td> <?php echo $data['tanggal_kembali'];?> </td>
																<td> <?php echo $data['status_peminjaman'];?> </td>
																<td> <?php echo $data['nama_pegawai'];?> </td>
																
															</tr>	
														<?php 
														}
														?>
											</tbody>
								</table>  
								<script type="text/javascript" src="assets/js/jquery.min.js"></script>
									<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
									<script>$(document).ready(function(){
										$('#example').DataTable();
									});
									</script>
								
								</div>
					
							</div>
							
							
				
                            </div>
							
                            <!--/.module-->
                        </div>
                        <!--/.content-->
                    </div>
                    </div>
                       
           
             </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>